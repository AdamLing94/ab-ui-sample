function isblank(str) {
    return (!str || /^\s*$/.test(str));
}

function formvalidation() {
    if(isblank($("#mobile_number").val())) {
        $(".result#username").removeClass("hide");
        $(".result#username").text(window.empty);
    };

    if(isblank($("#passwords").val())) {
        $(".result#password").removeClass("hide"); 
        $(".result#password").text(window.empty);
    }else {
        window.uservalidation()
    }
}