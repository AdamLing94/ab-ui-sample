import 'third-party/jquery.min.js';
import 'third-party/intlTelInput.js';
import 'userValidation.js';
import 'functionReferences.js';
import 'frameDisplay.js';
import 'formValidation.js';
import 'formatValidation.js';
import 'messageReferences.js';