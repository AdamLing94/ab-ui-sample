/* Validation */

/**
 * @desc   Check blank
 * @param  {String}  str
 * @author Jianping
 * @return {Boolean} 
 */
function isBlank(str) {
  return (!str || /^\s*$/.test(str));
}
/**
 * @desc   Check email formate
 * @param  {String}  str
 * @author Jianping
 * @return {Boolean} 
 */
function isEmail(str) {
  return /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/.test(str);
};

/**
 * @desc   Check URL formate
 * @param  {String}  str
 * @author Jianping
 * @return {Boolean} 
 */
function isUrl(str) {
  return /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/i.test(str);
};
