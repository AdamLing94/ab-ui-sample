/**
 * @desc   Only Allow Numeric Input
 * @param  {String} onlyAllowNumericInput
 * @author Jianping
 */
function onlyAllowNumericInput(inputElementId) {
    var textInput = document.getElementById(inputElementId);
    // called when key is pressed in keydown, we get the keyCode in keyup, we get the input.value (including the charactor we've just typed
    textInput.addEventListener("keydown", function _OnNumericInputKeyDown(e) {
        var key = e.which || e.keyCode; // http://keycode.info/
        if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
            // alphabet
            key >= 65 && key <= 90 ||
            // spacebar
            key == 32) {
            e.preventDefault();
            return false;
        }
        if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
            // numbers
            key >= 48 && key <= 57 ||
            // Numeric keypad
            key >= 96 && key <= 105 ||
            // allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // allow: Ctrl+C
            (key == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (key == 88 && e.ctrlKey === true) ||
            // allow: home, end, left, right
            (key >= 35 && key <= 39) ||
            // Backspace and Tab and Enter
            key == 8 || key == 9 || key == 13 ||
            // Del and Ins
            key == 46 || key == 45) {
            return true;
        }
        var v = this.value; // v can be null, in case textbox is number and does not valid
        // if minus, dash 
        if (key == 109 || key == 189) {
            // if already has -, ignore the new one
            if (v[0] === '-') {
                // console.log('return, already has - in the beginning');
                return false;
            }
        }
        if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
            // comma, period and numpad.dot
            key == 190 || key == 188 || key == 110) {
            // console.log('already having comma, period, dot', key);
            if (/[\.,]/.test(v)) {
                // console.log('return, already has , . somewhere');
                return false;
            }
        }
    });
}